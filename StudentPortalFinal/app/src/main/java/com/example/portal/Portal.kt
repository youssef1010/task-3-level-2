package com.example.portal

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Portal(
    var reminderText: String
    ,var url:String
) :Parcelable
